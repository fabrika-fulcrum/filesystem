<?php
namespace Fulcrum\Filesystem;

use Fulcrum\Filesystem\Exception\InvalidPathOperationException;
use Fulcrum\Filesystem\Exception\IOException;
use Fulcrum\Filesystem\Exception\PathNotInWebRootException;
use Fulcrum\Filesystem\Exception\PermissionException;
use FilesystemIterator;
use RecursiveDirectoryIterator;
use RecursiveIteratorIterator;

class Path implements \JsonSerializable
{
    protected $is_empty = false;
    protected $is_dir = false;
    protected $is_absolute = false;
    protected $is_file = false;

    protected $directory = '';
    protected $basename = '';
    protected $filename = '';
    protected $extension = '';

    protected static $dirCache = [];

    /**
     * Create a Path object from a string, automatically determine if directory based on trailing slash
     *
     * @param string $pathStr
     * @return Path
     */
    public static function Create($pathStr)
    {
        if (func_num_args() > 1) {
            $pathStr = implode('/', func_get_args());
        }
        return new Path($pathStr);
    }

    /**
     * Create a Path object from a string, force as directory
     *
     * @param $pathStr
     * @return Path
     */
    public static function CreateDir($pathStr)
    {
        if (func_num_args() > 1) {
            $pathStr = implode('/', func_get_args());
        }
        return new Path($pathStr, true);
    }

    /**
     * @param string $pathStr
     * @param bool $as_directory treat path as directory
     */
    public function __construct($pathStr, $as_directory = false)
    {
        $hasLeadingSlash = substr($pathStr, 0, 1) === '/';
        $hasTrailingSlash = mb_substr($pathStr, -1) === '/';

        $this->is_dir = $pathStr == '' || $hasTrailingSlash || $as_directory;
        $this->is_absolute = $hasLeadingSlash;

        $parts = explode('/', $pathStr);

        $pathParts = [];
        foreach ($parts as $part) {
            if ($part != '') {
                if ($part == '..') {
                    array_pop($pathParts);
                } else if ($part != '.') {
                    $pathParts[] = $part;
                }
            }
        }

        if (!$this->is_dir) {
            $filePart = array_pop($pathParts);
            $this->is_file = true;

            $basename = $filePart;
            $fileParts = explode('.',$basename);
            $extension = '';
            if (count($fileParts)>0) {
                if (count($fileParts)>1) {
                    $extension = array_pop($fileParts);
                }
            }

            $filename = implode('.',$fileParts);
            if ($filename == '' && $extension != '') {
                $filename = '.'.$extension;
                $extension = '';
            }
            $this->basename = $basename;
            $this->filename = $filename;
            $this->extension = $extension;
        }

        $this->directory = ($hasLeadingSlash?'/':'').implode('/',$pathParts).(count($pathParts)>0?'/':'');
        if ($this->directory == '//') {
            $this->directory = '/';
        }
    }

    /**
     * Determine if path is a directory
     *
     * @return bool
     */
    public function isDirectory()
    {
        return $this->is_dir;
    }

    /**
     * Determine if path is absolute (starts with a leading slash)
     * @return bool
     */
    public function isAbsolute(){
        return $this->is_absolute;
    }

    /**
     * Determine if path is a file
     *
     * @return bool
     */
    public function isFile(){
        return $this->is_file;
    }

    /**
     * Get/Set Basename (filename.ext)
     *
     * @param null $baseName
     * @return Path|mixed|string
     */
    public function basename($baseName = null){
        if ($baseName === null) {
            return Path::Create($this->basename);
        }
        return new Path($this->directory.$baseName);
    }

    /**
     * Get/Set extension
     *
     * @param null $ext
     * @return Path|mixed|string
     */
    public function extension($ext = null){
        if ($ext === null) {
            return $this->extension;
        }
        return new Path($this->directory.$this->filename.($ext?'.'.$ext:''));
    }

    /**
     * Get/Set filename (without extension)
     *
     * @param null $filename
     * @return Path|mixed|string
     */
    public function filename($filename = null){
        if ($filename === null) {
            return $this->filename;
        }
        return new Path($this->directory.$filename.($this->extension?'.'.$this->extension:''));
    }

    /**
     * Get/Set Directory of path, without modifying the filename and extension
     *
     * @param null $directory
     * @return Path|string
     */
    public function directory($directory=null){
        if ($directory === null) {
            return Path::CreateDir($this->directory);
        }
        return new Path($directory.'/'.$this->filename.($this->extension?'.'.$this->extension:''));
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->directory.$this->basename;
    }

    /**
     * Append a directory or filename to current directory. Throws exception if applied to a file path
     *
     * @param string $str
     * @return Path
     * @throws InvalidPathOperationException;
     */
    public function append($str)
    {
        if ($this->is_file) {
            throw new InvalidPathOperationException('Cannot append a path after a filename');
        }
        if (func_num_args() > 1) {
            $str = implode('/', func_get_args());
        }
        return new Path($this->directory.'/'.$str);
    }

    /**
     * Append a directory to current directory. Treats appended string as directory. Throws exception if applied to a file path
     *
     * @param string $str
     * @return Path
     * @throws InvalidPathOperationException;
     */
    public function appendDir($str)
    {
        if ($this->is_file) {
            throw new InvalidPathOperationException('Cannot append a path after a filename');
        }
        if (func_num_args() > 1) {
            $str = implode('/', func_get_args());
        }
        return new Path($this->directory.'/'.$str.'/');
    }


    /**
     * Prepends a directory to current path
     *
     * @param string $str
     * @return Path
     */
    public function prepend($str){
        return new Path($str.'/'.$this->__toString());
    }


    /**
     * Determines if current path referst to a file inside the web root
     *
     * @return bool
     */
    public function inWebRoot(){
        return mb_substr($this->__toString(),0,mb_strlen($_SERVER['DOCUMENT_ROOT'])) == $_SERVER['DOCUMENT_ROOT'];
    }

    /**
     * Return absolute url from path that is inside web root. Throws exception if path is outside web root.
     *
     * @return Path
     * @throws InvalidPathOperationException
     * @throws PathNotInWebRootException
     */
    public function webPath(){
        if (!$this->inWebRoot()) {
            throw new PathNotInWebRootException();
        }
        return new Path('/'.$this->stemFrom($_SERVER['DOCUMENT_ROOT']));

    }

    /**
     * Removes the initial part of the path as defined by $str. Throws exception if $str does not match.
     *
     * @param $str
     * @return Path
     * @throws InvalidPathOperationException
     */
    public function stemFrom($str) {
        if (mb_substr($this,0,mb_strlen($str)) == $str) {
            return new Path(mb_substr($this,mb_strlen($str)));
        }
        throw new InvalidPathOperationException('Could not stem path from '.$str);
    }

    /**
     * Returns normalized array of path segments, removing leading and trailing slashes in all cases.
     * @return array
     */
    public function segments(){
        if ($this->isDirectory()) {
            return explode('/',substr($this->__toString(),$this->isAbsolute()?1:0, -1));
        }
        return explode('/',substr($this->__toString(),$this->isAbsolute()?1:0));
    }

    /**
     * Returns a specific segment of the path or null if it does not exist
     * @param int $index
     * @return string|null
     */
    public function segment($index) {
        $segments = $this->segments();
        return $segments[$index<0?(\count($segments)+$index):$index] ?? null;
    }

    /**
     * Check if path refers to an existing file/directory on disk
     * @return boolean
     */
    public function exists() {
        $result = true;
        if (!file_exists($this->__toString())) {
            $result = false;
            if ($this->is_dir && !is_dir($this->__toString())) {
                $result = false;
            }
        }
        return $result;
    }

    /**
     * Create directory recursively
     *
     * @param int $mode
     * @return $this
     * @throws IOException
     * @throws InvalidPathOperationException
     */
    public function mkdir($mode = 0775) {
        if (!$this->is_dir){
            throw new InvalidPathOperationException('Not a directory - cannot create');
        }
        if (!$this->isAbsolute()) {
            throw new InvalidPathOperationException('Path not absolute - cannot create');
        }
        if (!$this->exists()) {
            $result = @mkdir($this->directory(),$mode,true);
            if (!$result) {
                throw new IOException('Could not create '.$this->__toString());
            }
        }
        return $this;
    }

    /**
     * Write Contents to a file. If the path to the file does not exist, attempts to create it.
     *
     * @param $contents
     * @return $this
     * @throws IOException
     * @throws InvalidPathOperationException
     */
    public function write($contents) {
        if (!$this->isFile()) {
            throw new InvalidPathOperationException('Cannot write contents to a directory');
        }

        Path::CreateDir($this->directory())->mkdir();
        $result = file_put_contents($this,$contents);
        if ($result === false) {
            throw new IOException('Could not write to file');
        }
        return $this;
    }

    /**
     * Read Contents of file
     *
     * @return string
     * @throws IOException
     * @throws InvalidPathOperationException
     */
    public function read() {
        if (!$this->isFile()) {
            throw new InvalidPathOperationException('Cannot read contents of a directory');
        }
        if (!$this->exists()) {
            throw new IOException('File does not exist :'.$this->__toString());
        }
        return file_get_contents($this);

    }

    /**
     * Deletes a file or directory
     *
     * @throws IOException
     */
    public function delete($recursive = false){
        if ($this->isFile()) {
            if (!$this->exists()) {
                throw new IOException('File does not exist: '.$this->__toString());
            }
            $result = unlink($this);
            if (!$result) {
                throw new IOException('Could not Delete File: '.$this->__toString());
            }
        } else if ($this->isDirectory()) {
            if (!$this->exists()) {
                throw new IOException('Directory does not exist '.$this->__toString());
            }
            if ($recursive) {
                foreach( new RecursiveIteratorIterator(
                             new RecursiveDirectoryIterator( $this, FilesystemIterator::SKIP_DOTS | FilesystemIterator::UNIX_PATHS ),
                             RecursiveIteratorIterator::CHILD_FIRST ) as $value ) {
                    $value->isFile() ? unlink( $value ) : rmdir( $value );
                }
                $result = rmdir( $this );
            } else {
                $result = rmdir($this);
            }
            if (!$result) {
                throw new IOException('Could not Remove Directory '.$this->__toString());
            }
        }
    }

    /**
     * @param string $pattern
     * @throws InvalidPathOperationException
     * @return Path[]
     * @throws IOException
     */
    public function list($pattern = '*', $show_files=true, $show_dirs=true) {
        $result = [];
        if (!$this->isDirectory()) {
            throw new InvalidPathOperationException('Trying to list files in a non-directory '.$this);
        }
        if (!$this->exists()) {
            throw new InvalidPathOperationException('Directory does not exist '.$this);
        }
        //FIXME: Ditch use of glob in favor of a more memory-savvy method
        $globFlags = GLOB_MARK;
        if (!$show_files) {
            $globFlags = $globFlags|GLOB_ONLYDIR;
        }
        $matches = glob($this.$pattern, $globFlags);
        if ($matches === false) {
            throw new IOException('Error occured trying to get directory contents for '.$this);
        }
        foreach ($matches as $match) {
            $item = Path::Create($match);
            if ($item->isDirectory() && $show_dirs) {
                $result[] = $item;
            } else if ($item->isFile() && $show_files) {
                $result[] = $item;
            }
        }
        return $result;
    }

    /**
     * @param string $pattern
     * @return Path[]
     * @throws IOException
     * @throws InvalidPathOperationException
     */
    public function listFiles($pattern = '*') {
        return $this->list($pattern, true, false);
    }

    /**
     * @param string $pattern
     * @return Path[]
     * @throws IOException
     * @throws InvalidPathOperationException
     */
    public function listDirs($pattern = '*') {
        return $this->list($pattern, false, true);
    }

    //TODO: Move
    //TODO: Copy
    public function jsonSerialize()
    {
        return $this->__toString();
    }
}
